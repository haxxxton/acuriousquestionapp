var IAP = {};
var localStorage = window.localStorage || {};


IAP.initialize = function () {
    
    if(localStorage){
        logg('<pre>'+JSON.stringify(window.acq.localStore, undefined, 2)+'</pre>');
    }
    // Check availability of the storekit plugin
    if (!window.storekit) {
        return;
    }
    
    // Initialize
    storekit.init({
        ready:    IAP.onReady,
        purchase: IAP.onPurchase,
        restore:  IAP.onRestore,
        error:    IAP.onError
    });
};

IAP.onReady = function () {
    // Once setup is done, load all product data.
    storekit.load(window.acq.list, function (products, invalidIds) {
        
        for (var j = 0; j < products.length; ++j) {
            var p = products[j];
            
            if(window.acq.localStore.purchases.hasOwnProperty(p.id)){
                p.isPurchased = true;
            }else{
                p.isPurchased = false;
            }
            
            // update local content with store content
            for(var x = 0; x < window.acq.packs.length; x++){
                if(window.acq.packs[x].id == p.id){
                    if(p.isPurchased){
                        logg('already purchased '+p.title);
                    }else{
	                    addButton(p);
                    }
                    window.acq.packs[x].title = p.title;
                    window.acq.packs[x].price = p.price;
                    window.acq.packs[x].isPurchased = p.isPurchased;
                    window.acq.packs[x].description = p.description;
                }
            }
        }
        
        IAP.loaded = true;
        $(document).trigger('purchasesReady');
    });
};

IAP.onPurchase = function (transactionId, productId) {
    $(document).trigger('purchaseSuccess', [productId]);
    if (IAP.purchaseCallback) {
        IAP.purchaseCallback(productId);
        delete IAP.purchaseCallback;
    }
};

IAP.onError = function (errorCode, errorMessage) {
    if(errorCode === 4983503){
        // transaction cancelled
        logg('Transaction Cancelled');
        $(document).trigger('purchaseCancel');
    }else{
	    logg('Error: ' + errorMessage + ' errorCode: ' + errorCode);
        $(document).trigger('purchaseError');
    }
};

IAP.onRestore = function (transactionId, productId, transactionReceipt) {
    $(document).trigger('purchaseSuccess', [productId]);
};

IAP.buy = function (productId, callback) {
    // begin puchase process
    IAP.purchaseCallback = callback;
    storekit.purchase(productId);
};

IAP.restore = function () {
    storekit.restore();
};

logg = function (data) {
    // document.getElementById('logging').innerHTML += data + '<br />';
};
addButton = function(product){
    logg('<a href="javascript:void(0)" onclick="IAP.buy(\''+product.id+'\')">'+product.title+' : '+product.price+'</a>');
};