/*
 * 1) initialise global element variables
 * 2) attach events to elements and set up basic styling
 * 3) get local packs and questions data
 * 4) create list of paid packs
 * 5) load/create localStorage
 * 6) check internet connection
 * 7) detect platform and load appropriate sales plugin
 * 8) update descriptions, titles and buttons from returned store data
*/
(function (global) {
    var version = "0.0.0.4";
    var els = [];
    var data = {
        isAnimating : false,
        animateInTime: 400,
        animateOutTime: 400,
        pauseBetweenSlidesTime: 100,
        pTimingOffset: 70,
        rotateTo: 1,
        slideDistance: 80,
        upDistance: 200,
        currentSlide: 0,
        showRandomSpinner: false
    };
    window.acq = [];
    
    // creates default user settings
    function createDefaultLocalSettings(){
        window.acq.localStore = {
            "version":"0.0.0.4",
            "seenIntro":false,
            "seenSwipeInstruction":false,
            "startPack":0,
            "startQuestion":{},
            "purchases":{}
        };
        //if(window.device){
        //    window.acq.localStore.uuid = window.device.uuid;
        //};
        updateLocalStorage();
    }
    
    function findPackById($id){
        var pack = { color: "" };
        for(var x = 0; x < window.acq.packs.length; x++){
            if(window.acq.packs[x].id == $id){
                pack = window.acq.packs[x];
                break;
            }
        }
        return pack;
    }
    
    function updatePackContent($purchaseIndex, $el){
        var packContain = $el.parent();
        var pack = window.acq.packs[$purchaseIndex];
        var type = pack.type;
        var copy = '<h2>'+pack.title+'</h2>';
        copy += formatCopy(pack.description);
        if(type === 'bundle'){
            copy += '<ul class="bundle_icons">';
            for(var x = 0; x < pack.packs.length; x++){
                copy += '<li><i class="icon icon-'+classifyCopy(pack.packs[x])+'" style="background-color:'+findPackById(pack.packs[x]).color+';"></i></li>';
            }
            copy += '</ul>';
        }
        $el.find('.cell-center').html(copy);
    }
    
    // update swipe area content, classes and id
    function updateSwipeshowContent($packIndex, $el){
        var question = data.currentQuestions[$packIndex];
        var copy = question.content;
        var lineBreaks = countLBs(copy);
        if(lineBreaks){
            lineBreaks = lineBreaks.length;
        }else{
            lineBreaks = 0;
        }
        var formattedCopy = formatCopy(copy);
        $el.removeClass('lb-0 lb-1 lb-2 lb-3').addClass('lb-'+lineBreaks).attr('id', 'question-'+question.id);
        $el.find('.cell-center').html(formattedCopy);
        updateShareUrls(copy);
        return lineBreaks;
    }
    
    // updates the share urls in the footer
    function updateShareUrls($copy){
        var cleanCopy = formatShareCopy($copy);
        var fblink = shareReplacements(cleanCopy, els.fblink.data('url'));
        els.fblink.data('url', fblink);
        var twlink = shareReplacements(cleanCopy, els.twlink.data('url'));
        els.twlink.data('url', twlink);
        var emlink = shareReplacements(cleanCopy, els.emlink.data('url'));
        els.emlink.data('url', emlink);
    }
    
    // replaces sections in the share url string with their values
    function shareReplacements($copy, $link){
        $link = $link.replace(/\{summary\}/g, $copy);
        $link = $link.replace(/\{link\}/g, 'http://acuriousquestion.com/');
        $link = $link.replace(/\{title\}/g, 'A Curious Question');
        return $link;
    }
    
    // adds paragraph tags in around your mouth
    function formatCopy(copy){
        return '<p>'+copy.replace(/\|/g,'</p><p>')+'</p>';
    }
    
    // removes line break delimiter for sharing
    function formatShareCopy(copy){
        return copy.replace(/\|/g, ' ');
    }
    
    // return the number of line breaks in a block of copy
    function countLBs(copy){
        return copy.match(/\|/g);
    }
    
    // replace '.' with '-'
    function classifyCopy(copy){
        return copy.replace(/\./g, '-');
    }
    
    // handles the click of the pack button
    function openOrBuyPack(){
        var pack = window.acq.packs[window.acq.localStore.startPack];
        if(els.packButton.is('.open')){
            openPack(pack);
        }else{
            if(window.device.platform == "iOS"){
                if(IAP.loaded){
		            showPackButtonLoading();
                    IAP.buy(pack.id);
                }
            }else{
                // currently unsupported platform                
            }
        }
    }
    
    function openPack(pack){
        if(!window.acq.localStore.seenSwipeInstruction){
            window.acq.localStore.seenSwipeInstruction = true;
            updateLocalStorage();
        }
        data.currentQuestions = [];
        for(var x = 0; x < window.acq.questions.length; x++){
            if(pack.type == "bundle"){
                if(pack.packs.indexOf(window.acq.questions[x].pack) >= 0 ){
                    data.currentQuestions.push(window.acq.questions[x]);
                }
            }else{
                if(window.acq.questions[x].pack == pack.id){
                    data.currentQuestions.push(window.acq.questions[x]);
                }
            }
        }
        els.topDividerIcon.css({'background-color':pack.color}).attr('class','icon icon-'+classifyCopy(pack.id));
        els.packName.find('p').text(pack.title);
        transitionToSlides(false);
    }    
    function openDeck(){
        if(!window.acq.localStore.seenSwipeInstruction){
            window.acq.localStore.seenSwipeInstruction = true;
            updateLocalStorage();
        }
        data.currentQuestions = [];
        for(var x = 0; x < window.acq.questions.length; x++){
            for(var y = 0; y < window.acq.packs.length; y++){
                if(window.acq.packs[y].isPurchased && window.acq.packs[y].id == window.acq.questions[x].pack){
                    data.currentQuestions.push(window.acq.questions[x]);
                    break;
                }
            }
        }
        // if clicking the deck count from within a pack
        if(els.body.is('.state-questions')){
            if(window.acq.localStore.startQuestion.hasOwnProperty('deck')){
                doAnimation(window.acq.localStore.startQuestion.deck, -1, false);
            }else{
				goToRandomSlide();
            }
            els.packName.transition({
                opacity:0
            }, data.transitionOutTime);
            els.topDividerIcon.transition({
                opacity:0
            }, data.transitionOutTime);
            els.deckCount.find('.yourdeck').transition({
                opacity:'0'
            }, data.transitionOutTime, function(){
                els.packName.find('p').text('Your deck');
                els.deckCount.find('.yourdeck').text('Randomize ');
                setTimeout(function(){
                    els.deckCount.find('.yourdeck').transition({
                        opacity: 1
                    }, data.transitionInTime);
                    els.packName.transition({
                        opacity:1
                    }, data.transitionInTime);
                }, data.pauseBetweenSlidesTime);
            });
        }else{
            els.packName.find('p').text('Your deck');
            transitionToSlides(true);
        }
    }
    
    function transitionToSlides(isDeck){
        els.packsIndicator.transition({
            opacity:0
        }, data.transitionOutTime, function(){
            els.packsIndicator.css({'display':'none'});
            els.shareLinks.css({'opacity':'0'});
        });
        els.slideInstruction.transition({
            opacity:0
        }, data.transitionOutTime);
        if(isDeck){
            els.deckCount.find('.yourdeck').transition({
                opacity:'0'
            }, data.transitionOutTime, function(){
                els.deckCount.find('.yourdeck').text('Randomize ');
            });
        }
        els.nextArrow.transition({
            right:'-100%'
        }, data.transitionOutTime);
        els.prevArrow.transition({
            left:'-100%'
        }, data.transitionOutTime, function(){
            setTimeout(function(){
                els.packContain.transition({
                    opacity:0,
                    y:-data.upDistance
                }, data.transitionOutTime, function(){
                    els.packContain.transition({
                        y:0
                    }, 0);
                    els.body.removeClass('state-packs').addClass('state-questions');
                    if(!isDeck){
                        els.topDividerIcon.css({'opacity':'1'});               
                    }else{
                        els.topDividerIcon.css({'opacity':'0'});
                    }
                    els.shareLinks.css({'display':'block'});
                    els.shareLinks.transition({
                        opacity:1
                    }, data.transitionInTime);
                    els.deckCount.find('.yourdeck').transition({
                        opacity:1
                    }, data.transitionInTime, function(){
                        els.slider.transition({
                            opacity:1
                        }, data.transitionInTime);
                        els.packName.transition({
                            opacity:1
                        }, data.transitionInTime);
                        els.nextArrow.transition({
                            right:'0'
                        }, data.transitionInTime);
                        els.prevArrow.transition({
                            left:'0'
                        }, data.transitionInTime)
                        
                        // use localstore slide number
                        var startQuestionForCurrentPack = 0;
                        if(isDeck){
                            if(window.acq.localStore.startQuestion.hasOwnProperty('deck')){
                                startQuestionForCurrentPack = window.acq.localStore.startQuestion.deck;
                            }
                        }else{
                            var packId = classifyCopy(window.acq.packs[window.acq.localStore.startPack].id);
                            if(window.acq.localStore.startQuestion.hasOwnProperty(packId)){
                                startQuestionForCurrentPack = window.acq.localStore.startQuestion[packId];
                            }
                        }
                        doInitialAnimation(startQuestionForCurrentPack, -1);
                        data.currentSlide = startQuestionForCurrentPack;
                    });
                })
            }, data.pauseBetweenSlidesTime);
        });
        els.cover_launch.on('click', returnToPacks);
    }
    
    function returnToPacks(){
        els.cover_launch.off('click', returnToPacks);
        els.shareLinks.transition({
            opacity:0
        }, data.transitionOutTime, function(){
            els.shareLinks.css({'display':'none'});
            els.packsIndicator.css({'opacity':'0'});
        });
        els.deckCount.find('.yourdeck').transition({
            opacity:'0'
        }, data.transitionOutTime, function(){
            els.deckCount.find('.yourdeck').text('Your Deck ');
        });
        els.packName.transition({
            opacity:0
        }, data.transitionOutTime);
        els.nextArrow.transition({
            right:'-100%'
        }, data.transitionOutTime);
        els.prevArrow.transition({
            left:'-100%'
        }, data.transitionOutTime, function(){
             setTimeout(function(){
                els.slider.transition({
                    opacity:0,
                    y:-data.upDistance
                }, data.transitionOutTime, function(){
                    els.slider.transition({
                        y:0
                    }, 0);
                    els.body.removeClass('state-questions').addClass('state-packs');
                    els.topDividerIcon.css({'opacity':'0'}); 
                    els.packsIndicator.css({'display':'block'});
                    els.packsIndicator.transition({
                        opacity:1
                    }, data.transitionInTime);
                    els.deckCount.find('.yourdeck').transition({
                        opacity:1
                    }, data.transitionInTime, function(){
                        els.packContain.transition({
                            opacity:1
                        }, data.transitionInTime);
                        els.nextArrow.transition({
                            right:'0'
                        }, data.transitionInTime);
                        els.prevArrow.transition({
                            left:'0'
                        }, data.transitionInTime)
                        els.slider.find('.slide').find('.cell-center').empty();
                        els.slider.find('.slide').attr('id','').removeClass('lb-0 lb-1 lb-2 lb-3 lb-4');
                    });
                })
            }, data.pauseBetweenSlidesTime);
        });
    }
    
    // user swiped right
    function slideRight(){
        if(data.isAnimating == false){
            if(els.body.is('.state-packs')){
                triggerPacks(-1);
            }else{
                triggerSlide(-1);
            }
        }
    }
    
    // user swiped left
    function slideLeft(){
        if(data.isAnimating == false){
            if(els.body.is('.state-packs')){
                triggerPacks(1);
            }else{
                triggerSlide(1);
            }
        }
    }
    
    function triggerPacks($direction){
        $purchaseIndex = false;
        // check if we are at the beginning or end of the pack list
        if(window.acq.localStore.startPack - $direction >= 0 && window.acq.localStore.startPack - $direction < window.acq.packs.length){
            // procede as normal
            $purchaseIndex = window.acq.localStore.startPack - $direction;
        }else if(window.acq.localStore.startPack - $direction < 0){
            // we are trying to go back from the first slide so show the last in the list
            $purchaseIndex = window.acq.packs.length - 1;
        }else{
            // we are trying to go after the last slide so show the first in the list
            $purchaseIndex = 0;
        }
        doPackAnimation($purchaseIndex, $direction);
        
        window.acq.localStore.startPack = $purchaseIndex;
        // update localStore
        updateLocalStorage();
    }
    
    // rotate the curr and next slide datas
    function triggerSlide($direction){
        $packIndex = false;
        // check if we are at the beginning or end of the pack list
        if(data.currentSlide - $direction >= 0 && data.currentSlide - $direction < data.currentQuestions.length){
            // procede as normal
            $packIndex = data.currentSlide - $direction;
        }else if(data.currentSlide - $direction < 0){
            // we are trying to go back from the first slide so show the last in the list
            $packIndex = data.currentQuestions.length - 1;
        }else{
            // we are trying to go after the last slide so show the first in the list
            $packIndex = 0;
        }
        data.currentSlide = $packIndex;
        if(els.deckCount.find('.yourdeck').text() == 'Randomize '){
            window.acq.localStore.startQuestion.deck = $packIndex;
        }else{
            var packId = classifyCopy(window.acq.packs[window.acq.localStore.startPack].id);
	        window.acq.localStore.startQuestion[packId] = $packIndex;
        }
        updateLocalStorage();
        // perform animation
        doAnimation($packIndex, $direction, false);
    }
    
    function doPackAnimation($purchaseIndex, $direction){
        updatePackContent($purchaseIndex, els.nextPack);        
        data.isAnimating = true;
        var tempPack = els.currPack;
        // fade out current slide and remove its class
        els.currPack.transition({
            opacity:'0', 
            scale:0.8, 
            x: $direction*300
        }, data.animateOutTime);
        els.packContain.transition({
            opacity:0
        }, data.animateOutTime, function(){
            updatePackIconAndButton($purchaseIndex);
            updatePacksIndicator($purchaseIndex);
            setTimeout(function(){
                els.packContain.transition({
                    opacity:1
                }, data.animateInTime);
                
                $(this).removeClass('current');
                // add the current class to the old next slide and remove 'next' class
                els.nextPack.addClass('current')
                .removeClass('next')
                .css({
                    scale:0.8, 
                    x: $direction*-300
                })
                .transition({
                    opacity:'1', 
                    scale:'1', 
                    x: 0
                }, data.animateInTime, function(){
                    // reassign current slide
                    els.currPack = els.nextPack;
                    // add next class and fade in temp stored slide
                    tempPack.addClass('next');
                    els.nextPack = tempPack;
                    // allow for next swipe
                    data.isAnimating = false;
                });
            }, data.pauseBetweenSlidesTime);
        });
    }
    
    // performs the animation of the slides, update localstore current slide
    function doAnimation($packIndex, $direction, $isRandom){
        // direction:
        // -1 = animate content towards the left
        // 1 = animate content towards the right
        
        var $newPs = updateSwipeshowContent($packIndex, els.nextSlide);
        var $curPs = els.currSlide.find('p').length - 1;
        // stop swipe spam
        data.isAnimating = true;
        // temp store the current slide
        var tempSlide = els.currSlide;
        
        // for each p in the current slide rotate, fade and scale with 50ms offset
        $(els.currSlide.find('p').get().reverse())
        .css({transformOrigin: (50-($direction*50))+'% 50%'})
        .each(function($outIndex){
            var $oldThis = $(this);
            setTimeout(function(){
                $oldThis.transition({
                    opacity:0,
                    rotate:($direction*data.rotateTo)+'deg',
                    x:$direction*data.slideDistance
                }, data.animateOutTime, function(){
                    if($isRandom){                    
		                showRandomizeAnimation();
                    }
                    
                    // if we're talking about the last p
                    if($outIndex == $curPs){
                        els.currSlide.css({opacity:0});
                        els.currSlide.removeClass('current');
                        els.nextSlide.css({opacity:1});
                        if($isRandom){
                            setTimeout(function(){
			                    hideRandomizeAnimation();
                                animateSlideIn(tempSlide, $direction, $newPs);
                            }, data.animateInTime*2);
                        }else{
                        	animateSlideIn(tempSlide, $direction, $newPs);
						}
                    }
                });
            }, $outIndex * data.pTimingOffset);
        });
    }
              
  	function animateSlideIn(tempSlide, $direction, $newPs){
        setTimeout(function(){
            // add the current class to the old next slide and remove 'next' class
            els.nextSlide.addClass('current')
            .removeClass('next')
            .find('p')
            .css({transformOrigin: (50+($direction*50))+'% 50%',opacity:0,x:-$direction*data.slideDistance,rotate:(-$direction*data.rotateTo)+'deg'})
            .each(function($inIndex){
                var $newThis = $(this);
                setTimeout(function(){
                    $newThis.transition({
                        opacity:1,
                        rotate:0,
                        x:0
                    }, data.animateInTime, function(){
                        // if we're talking about the last p
                        if($inIndex == $newPs){
                            // reassign current slide
                            els.currSlide = els.nextSlide;
                            // add next class and fade in temp stored slide
                            tempSlide.addClass('next');
                            els.nextSlide = tempSlide;
                            // allow for next swipe
                            data.isAnimating = false;
                        }
                    });
                }, $inIndex * data.pTimingOffset);
            });
        }, data.pauseBetweenSlidesTime);
    }
    
    function showRandomizeAnimation(){            
        data.showRandomSpinner = true;
        startLoadingSpinner();
        els.randomIconContainer.stop().css({'z-index':1000}).transition({
            opacity:1
        }, data.animateInTime);
    }
    
    function hideRandomizeAnimation(){
        els.randomIconContainer.stop().css({'z-index':-1}).transition({
            opacity:0
        }, data.animateOutTime, function(){
	        stopLoadingSpinner();
        });
    }
    
    function updatePackIconAndButton($purchaseIndex){
        var pack = window.acq.packs[$purchaseIndex];
        els.packIcon.attr('class', 'icon icon-'+classifyCopy(pack.id)).css({'background-color': pack.color});
        if(pack.isPurchased){
            els.packButton.attr('class', 'open helvetica').find('span').html('Open <span class="icon icon-open">');
        }else{
            if(pack.price != 'Offline'){
                els.packButton.attr('class','helvetica').find('span').html('BUY '+pack.price);
            }else{
                els.packButton.attr('class','helvetica').find('span').html(pack.price);
            }
        }
    }
    
    // move current state along pack indicators
    function updatePacksIndicator($purchaseIndex){
        els.packsIndicator.find('.current').removeClass('current');
        els.packsIndicator.find('li').eq($purchaseIndex).addClass('current');    
    }
    
    // create pack indicators
    function loadPacksIndicator(){
        var packs = '';
        for(var x = 0; x < window.acq.packs.length; x++){
            packs += '<li><i data-pack="'+window.acq.packs[x].id+'" data-index="'+x+'" style="background-color:'+window.acq.packs[x].color+';"></i></li>';
        }
        els.packsIndicator.html(packs);
    }
    
    // performs the first animation of the packs
    function loadFirstPackDisplay($purchaseIndex){
        if(!window.acq.localStore.seenSwipeInstruction){
            els.slideInstruction.transition({
                'opacity':1
            }, data.animateInTime);
        }
        updatePackContent($purchaseIndex, els.currPack);
        updatePackIconAndButton($purchaseIndex);
        updatePacksIndicator($purchaseIndex);
        data.isAnimating = true;
        els.currPack
        .css({opacity:1});
        setTimeout(function(){
            els.nextArrow.transition({
                right:'0'
            }, data.animateOutTime);
            els.prevArrow.transition({
                left:'0'
            }, data.animateOutTime, function(){
                data.isAnimating = false;
            })
        }, 100);
    }
    
    // performs the first animation of the slide
    function doInitialAnimation($packIndex){
        var $newPs = updateSwipeshowContent($packIndex, els.currSlide);
        // stop swipe spam
        data.isAnimating = true;
        els.main.removeClass('loading');
        
        els.currSlide
        .css({opacity:1})
        .find('p')
        .css({transformOrigin:'100% 50%',opacity:0,x:data.slideDistance,rotate:(-data.rotateTo)+'deg'})
        .each(function($inIndex){
            var $newThis = $(this);
            setTimeout(function(){
                $newThis.transition({
                    opacity:1,
                    rotate:0,
                    x:0
                }, data.animateInTime, function(){
                    // if we're talking about the last p
                    if($inIndex == $newPs){
                        // allow for next swipe
                        data.isAnimating = false;
                    }
                });
            }, $inIndex * data.pTimingOffset);
        });
    }
    
    // closes the intro/cover slide
    function closeWelcome(){
        els.welcome.find('.table-center').transition({
            opacity:0,
            y:-200
        }, data.animateOutTime, function(){
            els.welcome.transition({
                opacity:0
            }, data.animateOutTime, function(){
                els.welcome.removeClass('current');
                window.acq.localStore.seenIntro = true;
                updateLocalStorage();
            });
        });
    }
    
    // generates random slide number
    function goToRandomSlide(){
        if(!data.isAnimating){
            var id = Math.floor(Math.random() * data.currentQuestions.length);
            if(id != data.currentSlide){
                data.currentSlide = id;
                doAnimation(id, -1, true);
                
                window.acq.localStore.startQuestion.deck = id;
                updateLocalStorage();
            }else{
                goToRandomSlide();
            }
        }
    }
    
    function startLoadingSpinner(){
        var frameCount = 24;
        var backTop = parseInt(els.randomIconContainer.css('background-position').split(' ')[1]);
        if(backTop > (frameCount -1)*(data.spinnerWidth * -1)){
            els.randomIconContainer.css({'background-position':'0px ' + (backTop - data.spinnerWidth) + 'px'});
        }else{
            els.randomIconContainer.css({'background-position':'0px 0px'});
        }
        if(data.showRandomSpinner){
            setTimeout(function(){
                startLoadingSpinner();
            }, 150);
        }
    }
    
    function stopLoadingSpinner(){
        data.showRandomSpinner = false;
    }
    
    // updates the localstorage value for acq with the current JSON array
    function updateLocalStorage(){
        var temp = window.acq.localStore;
        localStorage.acq = JSON.stringify(temp);
    }
    
    // clears local user settings
    function killSettings(){
        delete window.acq.localStore;
        //localStorage.removeItem('acq');
        localStorage.clear();
        console.log('localstorage reset');
    }
    
    function loadPurchasesFromLocalStore(){
        for(var x = 0; x < window.acq.packs.length; x++){
            // if there is a localstorage purchased flag for this item
            if(window.acq.localStore.purchases[window.acq.packs[x].id]){
                window.acq.packs[x].isPurchased = true;
                if(window.acq.packs[x].type == 'bundle'){
                    // if this item is a bundle, set all its packs to purchased
                    var bundlePacks = window.acq.packs[x].packs;
                    for(var y = 0; y < window.acq.packs.length; y++){
                        if(window.acq.packs[y].id.indexOf(bundlePacks) >= 0){
                            window.acq.packs[y].isPurchased = true;
                        }
                    }
                }
            }
        }
        updatePackContent(window.acq.localStore.startPack, els.currPack);
        updatePackIconAndButton(window.acq.localStore.startPack);
        updateDeckCount();
    }
    
    function checkConnectionAndLoadPurchasingPlugin(){
        // make sure we're connected to the internet when we try to initialise IAP
        if(navigator.connection.type.toLowerCase() != "none"){
            if(window.device.platform == "iOS"){
                IAP.initialize();
            }else{
                // currently unsupported platform
            }
        }else{
            $(document).on('online', function(){
                checkConnectionAndLoadPurchasingPlugin();
            });
        }
        loadPurchasesFromLocalStore();
    }
    
    function hidePackButtonLoading(){
        els.packButton.removeClass('loading');
    }
    function showPackButtonLoading(){
        els.packButton.addClass('loading');
    }
    
    function updateDeckCount(){
        var currentCount = els.deckCount.find('span.counter').text();
        var deckCount = 0;
        for(var x = 0; x < window.acq.packs.length; x++){
            if(window.acq.packs[x].type != 'bundle' && window.acq.packs[x].isPurchased){
                deckCount += window.acq.packs[x].qcount;
            }
        }
        if(deckCount != currentCount){
            els.deckCount.find('span.counter').transition({
                opacity:0
            }, data.animateOutTime, function(){        
		        els.deckCount.find('span.counter').text(deckCount);
                setTimeout(function(){
                    els.deckCount.find('span.counter').transition({
                        opacity:1
                    }, data.animateInTime)
                }, data.pauseBetweenSlidesTime);
            });
        }
    }

    function init() {
        els.document.on('purchasesReady', function(){
            updatePackContent(window.acq.localStore.startPack, els.currPack);
            updatePackIconAndButton(window.acq.localStore.startPack);
        });
        els.document.on('purchaseSuccess', function(event, productId){
            var pack = findPackById(productId);
            pack.isPurchased = true;
            window.acq.localStore.purchases[pack.id] = 1;
            if(pack.type == 'bundle'){
                for(var x = 0; x < pack.packs.length; x++){
                    for(var y = 0; y < window.acq.packs.length; y++){
                        if(pack.packs[x] == window.acq.packs[y].id){
                            window.acq.packs[y].isPurchased = true;
                            window.acq.localStore.purchases[pack.packs[x]] = 1;
                        }
                    }
                }
            }
            updateLocalStorage();
            updatePackContent(window.acq.localStore.startPack, els.currPack);
            updatePackIconAndButton(window.acq.localStore.startPack);
            hidePackButtonLoading();
            updateDeckCount();
        });
        els.document.on('purchaseCancel purchaseError', function(){
           	hidePackButtonLoading(); 
        });
        
        els.currSlide.css({transformOrigin: '50% 50%',
                    opacity: 0});
        els.nextSlide.css({transformOrigin: '50% 50%'});
        
        els.slider.swipe({
           swipeLeft:function(e){
               slideRight();
               e.stopPropagation();
           },
            swipeRight:function(e){
                slideLeft();
                e.stopPropagation();
            },
            threshold:40 
        });
        els.packs.swipe({
           swipeLeft:function(e){
               slideRight();
               e.stopPropagation();
           },
            swipeRight:function(e){
                slideLeft();
                e.stopPropagation();
            },
            threshold:40 
        });
        els.cover.swipe({
            swipe:function(e){
               closeCover();
               e.stopPropagation();
           },
            threshold:40 
        });
        els.nextArrow.swipe({
            tap:function(e){
                slideRight(); 
                e.stopPropagation();
            }
        });
        els.prevArrow.swipe({
            tap:function(e){
                slideLeft(); 
                e.stopPropagation();
            }
        });
        
        els.external_links.on('tap', function(e){
            var targetURL = $(this).data("url");
            window.open(targetURL, "_system");
            e.preventDefault();
        });
        
        els.packButton.swipe({
            tap:function(e){
                openOrBuyPack();
                e.stopPropagation();
            }
        });
        els.deckCount.swipe({
            tap:function(){
                if(els.deckCount.find('.yourdeck').text() == 'Randomize '){
                    goToRandomSlide();
                }else{
                    openDeck();
                }
            }
        });
        els.topDividerIcon.swipe({
            tap:function(){
                returnToPacks(); 
            }
        });
        els.packIcon.swipe({
            tap:function(){
                if(els.packButton.is('.open')){
                    els.packButton.swipe('tap');
                }
            }
        });
        els.packsIndicator.on('tap', 'i', function(){
            var $purchaseIndex = $(this).data('index');
			var $direction = 1;
            if($purchaseIndex != window.acq.localStore.startPack){
                if($purchaseIndex < window.acq.localStore.startPack){
                    $direction = -1;
                }
                doPackAnimation($purchaseIndex, $direction);
                window.acq.localStore.startPack = $purchaseIndex;
                // update localStore
                updateLocalStorage();
            }
        });
        els.welcome.swipe({
           swipeLeft:function(e){
                closeWelcome();
               	e.stopPropagation();
           },
            swipeRight:function(e){
                closeWelcome();
                e.stopPropagation();
            },
            threshold:40 
        });
        els.welcomeClose.swipe({
            tap:function(e){
                e.preventDefault();
                closeWelcome();
            }
        });
        
        if(els.window.outerHeight() > 500){
            // android tab or larger
            data.device = 'tablet';
            data.spinnerWidth = 100;
            data.slideDistance = data.slideDistance * 1.5;
            data.upDistance = data.upDistance * 1.5;
        }else{
            // iphone
            data.device = 'phone';
            data.spinnerWidth = 80;
        }
        
        $.getJSON('scripts/pack.json', function(data){ 
            window.acq.packs = data.packs; 
            window.acq.questions = data.questions;
            window.acq.list = [];
            // create list
            for(var x = 0; x < window.acq.packs.length; x++){
                if(window.acq.packs[x].type != "free"){
                    window.acq.list.push(window.acq.packs[x].id);
                }
                window.acq.packs[x].qcount = 0;
            }
            for(var q = 0; q < window.acq.questions.length; q++){
                if(window.acq.questions[q].isVisible == true){
                    for(var p = 0; p < window.acq.packs.length; p++){
                        if(window.acq.questions[q].pack == window.acq.packs[p].id){
                            window.acq.packs[p].qcount++;
                            break;
                        }
                    }
                }
            }
            
            if(localStorage.acq){
                // attempt parse of localstorage JSON
                try {
                    window.acq.localStore = JSON.parse(localStorage.acq);
                } catch(e){
                    createDefaultLocalSettings();
                };
                // check version matching
                if(window.acq.localStore.version != version){
                    var currentPurchases = window.acq.localStore.purchases;
                    createDefaultLocalSettings();
                    window.acq.localStore.purchases = currentPurchases;
                    updateLocalStorage();
                }
            }else{
                // no local data found
                createDefaultLocalSettings();
            }
            
            loadPacksIndicator();
            loadFirstPackDisplay(window.acq.localStore.startPack);
            
            checkConnectionAndLoadPurchasingPlugin();

            
            if(!window.acq.localStore.seenIntro){
                els.welcome.addClass('current');
            }
            els.main.addClass('current');
        });
    }

    $(document).on('deviceready', function(){
        // globals
        els.wrap = $('#wrap');
        els.external_links = $('a[rel="external"]');
        els.window = $(window);
        els.document = $(document);
        els.body = $('body');
        els.nextArrow = $('#nextArrow');
        els.prevArrow = $('#prevArrow');
        els.randomIconContainer = $('#random-icon-container');
        els.welcomeClose = $('#welcomeClose');
        els.slideInstruction = $('#slideInstruction');
        
        // swipe slide
        els.welcome = $('#welcome');
        els.main = $('#main');
        els.slider = $('#qslider');
        els.currSlide = els.slider.find('.current');
        els.nextSlide = els.slider.find('.next');
        els.packs = $('#pslider');
        els.currPack = els.packs.find('.current');
        els.nextPack = els.packs.find('.next');
        els.shareLinks = $('#sharelinks');
        els.twlink = $('#twitter a');
        els.fblink = $('#facebook a');
        els.emlink = $('#email a');
        els.deckCount = $('#deck_count');
        els.packsIndicator = $('#packsIndicator');
        els.packButton = $('#pbutton');
        els.packIcon = $('#picon');
        els.packContain = $('#pcontain');
        els.topDividerIcon = $('#top-divider-icon');
        els.packName = $('#packName');
        
        // cover slide
        els.cover_launch = $('#cover_launch');
        els.devices = $('#devices');
        els.cover = $('#cover');
        els.cover_logo = $('#cover_logo');
        els.coverContents = $('#cover > *').not('#cover_logo');
        
        window.updateLocalStorage = function(){updateLocalStorage()};
        
        init();
    });

})(window, IAP);